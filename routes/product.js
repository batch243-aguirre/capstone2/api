const express = require("express");
const router = express.Router();
const Product = require("../models/Product.js");
const productController = require("../controllers/productController.js");
const auth = require("../auth.js");
const multer = require("multer");

const storage = multer.diskStorage({
	destination: function(req, file, cb){
		cb(null, './uploads/');
	},
	filename: function(req, file, cb){
		cb(null, new Date().toISOString().replace(/:/g, '-') + file.originalname)
	}
});

const fileFilter = (req, file, cb) => {
	// reject  file
	if (file.mimetype ==='image/png' || file.mimetype == 'image/jpg' || file.mimetype == 'image/JPG')
	cb(null, true);
	else {
		cb(null, false);
	}
	
}


const upload = multer({
	storage: storage,
	limits: {
		fileSize: 1024 * 1024 * 5
	},
	fileFilter : fileFilter
});


router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => {
		res.send(resultFromController)
	})
})

// create product (admin only)
router.post("/create", auth.verify, upload.single('productImage'), (req, res) => {
	const data = {
		product: req.body,
		productImage: req.file.path,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	productController.createProduct(data).then(resultFromController => { res.send(resultFromController)});
})

// retrieve all active products
router.get("/active", (req, res) => {
	productController.getActiveProducts().then(resultFromController => res.send(resultFromController))
})

router.get("/basketball", (req, res) => {
	productController.getBasketball().then(resultFromController => res.send(resultFromController))
})

router.get("/running", (req, res) => {
	productController.getRunning().then(resultFromController => res.send(resultFromController))
})

router.get("/sneaker", (req, res) => {
	productController.getSneaker().then(resultFromController => res.send(resultFromController))
})

// retrieve single product
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params.productId).then(resultFromController => {
		res.send(resultFromController)
	})
})

// update product info (admin only)
router.put("/:productId/update", auth.verify, (req, res) => {

	const data = {
		product : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	productController.updateProduct(req.params.productId, data).then(resultFromController => {
		res.send(resultFromController)
	})
})

// arhive product (admin only)
router.put("/:productId/archive", auth.verify, (req, res) => {

	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	productController.archiveProduct(req.params.productId, data).then(resultFromController => {
		res.send(resultFromController)
	})
})

router.put("/:productId/unarchive", auth.verify, (req, res) => {

	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	productController.unarchiveProduct(req.params.productId, data).then(resultFromController => {
		res.send(resultFromController)
	})
})

module.exports = router;